# Banking Made Simple for HNIs

Mr. Rao is 52 years old having asset of $20 Million USD and busy managing his small business. He has a dedicated Wealth Manager (WM) for investment advice, financial planning & banking services. They meet in the bank branch office or home as per their conversation. Mr. Rao feels that checking their schedule over phone is trivial in this digital age.

In this challenge, you need to build a solution to let WM book an appointment with HNIs like Mr. Rao. Mr. Rao can accept, reject  or suggest new time for the appointment. He can also use the same platform to book an appointment and be notified 30 minutes before the meeting.Also, a dashboard for WM to track the upcoming request, appointments and user profiles.

## Seed Data
branch.json : Sample data of DBS branches in India